<?php
class car extends vehicle {

private $model;
public function __construct($color,$model){
    parent::setColor($color);
    parent::setWheels(4);
    $this->model=$model;
}

public function setModel($model){
    $this->model=$model;
    }
public function getModel(){
        return $this->model;
    }
   

}
?>