<?php
class train{
    private $color;
    private $type;

     public function __construct($color,$type){
     $this->color= $color;
     $this->type=$type;
    }

    public function setColor($color){
    $this->color=$color;
    }
    public function getColor(){
        return $this->color;
    }
   
    public function setType($type){
     $this->type=$type;
    }
    public function getType(){
     return $this->type;
    }
    public function about(){
        echo "Train color is: ". $this->getColor(). "<br>";
        echo "Type: ". $this->getType(). "<br>";
    }
}

?>